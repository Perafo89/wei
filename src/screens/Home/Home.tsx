import React, { useEffect, useState, useContext } from 'react'
import './Home.css'

import { IonPage, IonText, IonButton, IonLabel, IonList, IonItem, IonAvatar, IonImg } from '@ionic/react'

import { Cat } from 'interfaces/cat'

import { AppContext } from 'store/appState'

const Home = () => {

  const { cats, getDbInfo, getAllDocs } = useContext(AppContext)

  const [counter, setCounter] = useState(0)
  const [docs, setDocs] = useState<Cat[]>([])

  useEffect(() => { 
    setupDb()   
    // setupCats()
    // console.log(JSON.stringify(cats))
  }, [])

  useEffect(() =>{
    // console.log(JSON.stringify(cats))
    console.log(cats)
    if(cats && Array.isArray(cats)){
      setDocs(cats)
    }
  }, [cats])

  const setupDb = async () => {   
    if(getDbInfo){
      try{
        const info = await getDbInfo()
        console.log('db info ' + JSON.stringify(info))
      }catch(e){
        console.error('error to initialize db ' + e)
      }
    }
  }

  const setupCats = async () => {
    if(getAllDocs){
      try{
        const cats = await getAllDocs()
        console.log(cats)
        setDocs(cats)
      }catch(e){  
        console.error(e)
      }
    }
  }

  return(
    <IonPage>
      <IonText>Perafo Inc - IglooLab</IonText>

      <IonButton 
        routerLink={'/form'}>
          Formulario
      </IonButton>

      <IonList>
        {docs.map((item: Cat, index) => (
          <IonItem key={index}>
            {item.image &&
              <IonAvatar>
                <img src={`data:image/jpeg;base64,${item.image}`} />
              </IonAvatar>
            }
            <IonLabel>{item.name}</IonLabel>
          </IonItem>
        ))
        }
      </IonList>
    </IonPage>
  )
}



export default Home
