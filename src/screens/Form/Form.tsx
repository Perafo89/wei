import React, { useEffect, useState, useContext } from 'react'
import './Form.css'

import { IonPage, IonContent, IonList, IonText, IonItem, IonInput, IonLabel, IonButton } from '@ionic/react'

import { AppContext } from 'store/appState'


const FormScreen = (props: any) => {

  const { getDbInfo, createOrUpdateDoc } = useContext(AppContext)

  const [catName, setCatName] = useState('')
  const [catPicture, setCatPicture] = useState<Blob>()
  const [mimeType, setMimeType] = useState<string | undefined>('')

  useEffect(() => { 
    setupDb()       
  }, [])

  const setupDb = async () => {   
    if(getDbInfo){
      try{
        const info = await getDbInfo()
        console.log('db info ' + JSON.stringify(info))
      }catch(e){
        console.error('error to initialize db ' + e)
      }
    }
  }

  const handleChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    if(e.target && e.target.files){
      const file: File = e.target.files[0]
      console.log('file image ' + file)


      const reader = new FileReader()
      reader.onload = (event) => {
        if(event && event.target){
          // console.log(event.target.result)
          const base64 = event.target.result
          const type = base64?.toString().split(';')[0].split('/')[1]
          const decodeBase64 = base64?.toString().split(',')[1]
          const blob = b64toBlob(decodeBase64)
          console.log(blob)
          setMimeType(type)
          setCatPicture(blob)
        }
      }
      reader.readAsDataURL(file)    
    }  

  }

  const b64toBlob = (b64Data: any, contentType = '', sliceSize = 512) => {
    const byteCharacters = atob(b64Data);
    const byteArrays = [];
 
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      const slice = byteCharacters.slice(offset, offset + sliceSize);
 
      const byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }
 
      const byteArray = new Uint8Array(byteNumbers);
      byteArrays.push(byteArray);
    }
 
    const blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  const handleSave = async () => {
    console.log('handle save ...')
    if(catName === ''){
      return
    }

    if(catPicture == null){
      return 
    }
    const doc = {
      _id: Date.now().toString(),
      name: catName,
      _attachments: {
        'picture': {
          content_type: `image/${mimeType}`,
          data: catPicture
        }
      }
    }

    console.log(JSON.stringify(doc))
    if(createOrUpdateDoc){
      try{
        await createOrUpdateDoc(doc)
        props.history.push('/home')
      }catch(e){
        console.error(e)
      }
    }
  }

  return(
    <IonPage>
      <IonContent>
        <IonItem>
          <IonInput value={catName} placeholder="Ingrese el Nombre" onIonChange={e => setCatName(e.detail.value!)}></IonInput>
        </IonItem>

        <input
          type="file"          
          accept="image/*"
          onChange={handleChange}
        />

        <IonButton onClick={handleSave}>Guardar</IonButton>
      </IonContent>     
      
    </IonPage>
  )
}

export default FormScreen