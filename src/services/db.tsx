import PouchDB from 'pouchdb'

const dbLocalName = 'cat'
const userName = 'admin'
const userPassword = 'admin'
const dbUrlLocal = `http://${userName}:${userPassword}@127.0.0.1:5984/cats`
const dbUrl = `http://${userName}:${userPassword}@192.168.1.101:5984/cats`

const syncOptions = {
  live: true,
  retry: true,
  continuous: true
}

let db: PouchDB.Database
let dbChanges

export const dbInitialize = () => {
  try{
    db = new PouchDB(dbLocalName)  
    db.sync(dbUrl, syncOptions)
  }catch(e){
    throw e
  }  
}

export const getDbInfo = async () => {
  try{
    const result = await db.info()
    return result
  }catch(e){
    throw e
  }  
}

export const watchDb = async () => {
  try{
    const changeOptions = {
      since: 'now',
      live: true,
      include_docs: true
    }
    
    dbChanges = db.changes(changeOptions)

    dbChanges.on('change', change => {
      console.log('changes ' + JSON.stringify(change.doc))
    })

    dbChanges.on('complete', complete => {
      console.log('db changes onComplete ' + complete)
    })

    dbChanges.on('error', e => {
      console.log('db changes on error ' + e)
    })
  }catch(e){
    throw e
  } 
}

export const getAllDocs = async () => {
  try{
    const result = await db.allDocs({include_docs: true, attachments: true })
    const documents = result.rows.map(item => {      
      let image = null
      if(item.doc?._attachments){
        image = item.doc._attachments['picture']
      }
      return {
        name: 'gato',
        image
      }
    })
    return documents
  }catch(e){
    throw e
  }  
}

export const getDocById = async (id: string) => {
  try{
     return await db.get(id) 
  }catch(e){
    throw e
  }
}

export const createOrUpdateDoc = async (doc: any) => {
  try{
    return await db.put(doc)
  }catch(e){
    throw e
  }
}

export const deleteDoc = async (doc: any) => {
  try{
    return await db.remove(doc)
  }catch(e){
    throw e
  }
}

export const getAttachment = async (doc: string, attachment: string) => {
  try{
    return await db.getAttachment(doc, attachment)
  }catch(e){
    throw e
  }
}

