
export interface Cat{
  name: string,
  image: string
}

export interface Att {
  content_type: string,
  data: string,
  digest: string
}