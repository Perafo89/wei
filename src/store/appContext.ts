import { createContext } from 'react'

const AppContext = createContext(null)

export const AppContextProvider = AppContext.Provider
export const AppContextConsumer = AppContext.Consumer