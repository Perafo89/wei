import React, { createContext, useReducer, useState, useEffect } from 'react'

import PouchDB from 'pouchdb'

import { ADD_CAT, REMOVE_CAT, ALLS_CATS } from './appTypes'

import { Cat, Att } from 'interfaces/cat'

import CONFIG from 'utils/config'

const dbUrl = `http://${CONFIG.dbUserName}:${CONFIG.dbuserPassword}@192.168.100.6:5984/cats`

interface IProviderProps {
  children?: React.ReactNode;
}
interface IInitialState {
  cats?: Array<Cat>,
  db?: PouchDB.Database,
  getDbInfo?: () => void,  
  createOrUpdateDoc?: (doc: any) => Promise<PouchDB.Core.Response>,
  getAllDocs?: () => Promise<any>,
}

const initialState : IInitialState = {
  cats: [],
}

let AppContext = createContext<IInitialState>({})

let reducer = (state: any, action: any) => {
  switch(action.type) {
    case ALLS_CATS: {
      return { ...state, cats: action.payload }
    }
  }
  return state
}

const AppContextProvider = (props: IProviderProps) => {
  const appInitialState = {
    ...initialState
  }

  let [state, dispatch] = useReducer(reducer, appInitialState)
  let [db, setDb] = useState(new PouchDB(CONFIG.dbLocalName))


  useEffect(() => {
    console.log('init app state provider')
    syncDatabase()
    watchDb()
    getAllDocs()
  }, [])


  const syncDatabase = () => {
    try{
      const sync = db.sync(dbUrl, CONFIG.syncOptions)
      .on('change', (info) => {
        // handle change
        console.log('sync db paused ' + info)
      }).on('paused', (e) => {
        // replication paused (e.g. replication up to date, user went offline)
        console.log('sync db paused ' + e)
      }).on('active', () => {
        // replicate resumed (e.g. new changes replicating, user went back online)
        console.log('sync db active')
      }).on('denied', (e) => {
        // a document failed to replicate (e.g. due to permissions)
        console.error('sync db denied ' + e)
      }).on('complete', (info) => {
        console.log('sync db complete ' + info)
      }).on('error', (e) => {
        console.error('sync db error ' + e)
      })
    }catch(e){
      console.error('error to sync database ' + e)
      throw e
    }  
  }

  const watchDb = async () => {
    try{
      const changeOptions = {
        since: 'now',
        live: true,
        include_docs: true
      }
      
      const dbChanges = db.changes(changeOptions)
  
      dbChanges.on('change', change => {
        console.log('changes ' + JSON.stringify(change.doc))
        getAllDocs()
      })
  
      dbChanges.on('complete', complete => {
        console.log('db changes onComplete ' + complete)
      })
  
      dbChanges.on('error', e => {
        console.log('db changes on error ' + e)
      })
    }catch(e){
      console.error('error to watch db changes ' + e)
      throw e
    } 
  }

  const getDbInfo = async () => {
    try{
      return await db.info() 
    }catch(e){
      throw e
    }  
  }

  const createOrUpdateDoc = async (doc: any) => {
    try{
      return await db.put(doc)
    }catch(e){
      throw e
    }
  }

  const getAllDocs = async () => {
    try{
      const result = await db.allDocs({include_docs: true, attachments: true })
      const documents = result.rows.map(item => {
        let image = null
        if(item.doc?._attachments){
          image = item.doc._attachments['picture'] as Att
        }
        
        return {
          ...item.doc,
          image: image?.data
        }       
      })
      console.log('get all documets app store ' + documents)
      dispatch({
        type: ALLS_CATS,
        payload: documents
      })

      return documents
      
    }catch(e){
      console.log('error to get all documents ' + e)
      throw e
    }  
  }


  return (
    <AppContext.Provider value={{ 
      cats: state.cats,
      getDbInfo,
      createOrUpdateDoc,
      getAllDocs
    }}>
      {props.children}
    </AppContext.Provider>
  )

}

let AppContextConsumer = AppContext.Consumer

export { AppContext, AppContextProvider, AppContextConsumer };

